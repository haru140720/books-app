db = db.getSiblingDB('books-app');
db.books.insert({
  Id: '0',
  Name: 'Harry Potter and the Half-Blood Prince (Harry Potter, #6)',
  RatingDist1: '1:9896',
  pagesNumber: 652,
  RatingDist4: '4:556485',
  RatingDistTotal: 'total:2298124',
  PublishMonth: 16,
  PublishDay: 9,
  Publisher: 'Scholastic Inc.',
  CountsOfReview: 28062,
  PublishYear: 2006,
  Language: 'eng',
  Authors: 'J.K. Rowling',
  Rating: 4.57,
  RatingDist2: '2:25317',
  RatingDist5: '5:1546466',
  ISBN: null,
  RatingDist3: '3:159960',
});

db.createUser({
  user: 'user',
  pwd: 'password',
  roles: [
    {
      role: 'readWrite',
      db: 'books-app',
    },
  ],
});
