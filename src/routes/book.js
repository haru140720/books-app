import { Router } from "express";
const router = Router();

import BookController from '../controller/book.js';
import transformReq from "../middleware/transform-req.js";

router.post('/search', BookController.searchBookByName);

router.post('/', BookController.getAllBook);

router.get('/getOne/:bookId', BookController.getBookById);

router.put('/updateOne/:bookId', BookController.updateBookById);

router.delete('/deleteOne/:bookId', BookController.deleteBookById);

export default router;