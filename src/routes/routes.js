import { API_VERSION } from '../constant/api-version.js';
import BookRouter from './book.js';
import BookBorrowRouter from './borrow-book.js';

export default function (app) {
    app.use(`/api/${API_VERSION.v1}/books`, BookRouter);
    //
    app.use(`/api/${API_VERSION.v1}/borrow-books`, BookBorrowRouter);
};

  