import { Router } from "express";
const router = Router();

import BookBorrowController from '../controller/book-borrow.js';

router.post('/', BookBorrowController.borrowBook);


export default router;