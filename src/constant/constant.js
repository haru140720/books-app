export const CONSTANT = {
    BOOK_BORROW_STATUS: {
        BORROWED: "BORROWED",
        EXPIRED: "EXPIRED",
        CANCELED: "CANCELED",
        RETURNED: "RETURNED"
    }
}