import mongoose from 'mongoose';

const BookSchema = new mongoose.Schema({
    Id: {
        type: String,
        required: true,
        index: { unique: true }
    },
    Name: {
        type: String,
        required: true,
        index: true
    },
    PublishDay: {
        type: Number,
        required: true
    },
    PublishMonth: {
        type: Number,
        required: true
    },
    PublishYear: {
        type: Number,
        required: true
    },
    Publisher: {
        type: String,
        required: true
    },
    CountsOfReview: {
        type: Number,
        required: true
    },
    Language: {
        type: String,
        required: true
    },
    Authors: {
        type: String,
        required: true
    },
    Rating: {
        type: Number,
        required: true
    },
    RatingDist1: {
        type: String,
        required: true
    },
    RatingDist2: {
        type: String,
        required: true
    },
    RatingDist3: {
        type: String,
        required: true
    },
    RatingDist4: {
        type: String,
        required: true
    },
    RatingDist5: {
        type: String,
        required: true
    },
    RatingDistTotal: {
        type: String,
        required: true
    },
    pagesNumber: {
        type: Number,
        required: true
    },
    ISBN: {
        type: String,
        default: null
    },
}, { versionKey: false, timestamps: true });

BookSchema.index({ Name: "text" });

export const BookModel = mongoose.model("Book", BookSchema);