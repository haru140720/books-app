import mongoose from 'mongoose';

export default function connectDb() {
    mongoose.set('strictQuery', false);
    const addr = process.env.NODE_ENV === "production" ? 'books-database' : 'localhost';
    const mongoUri = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${addr}:${process.env.MONGO_PORT}/books-app`;   
    mongoose.connect(mongoUri);
    console.log(mongoUri);
    mongoose.connection.on('error', err => {
      console.error('MongoDB connection error.');
      console.error(err);
      // process.exit();
    });
      
    mongoose.connection.once('open', () => {
      console.log(`Connected to MongoDB: ${mongoUri}`);
    });
}

