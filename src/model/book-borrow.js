import mongoose from 'mongoose';
import { CONSTANT } from '../constant/constant.js';

const BookBorrowItem = new mongoose.Schema(
    {
        bookId: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true,
        },
        authors: {
            type: String,
            required: true
        },
        rating: {
            type: Number,
            required: true
        },
        amount: {
            type: Number,
            required: true,
            default: 1
        }
    },
    {
        versionKey: false,
    }
);

const BookBorrowSchema = new mongoose.Schema(
    {
        books: {
            type: [BookBorrowItem],
            required: true
        },
        userId: {
            required: true,
            type: String,
            default: "user12345678"
        },
        expiredDate: {
            type: Number,
            required: true,
            index: true,
            default: new Date().getTime() + 30 * 24 * 60 * 60 * 1000
        },
        status: {
            type: String,
            enum: Object.values(CONSTANT.BOOK_BORROW_STATUS),
            required: true,
            index: true,
            default: CONSTANT.BOOK_BORROW_STATUS.BORROWED
        }
    },
    { 
        versionKey: false,
        timestamps: true 
    }
);

BookBorrowSchema.index({ userId: 1, status: 1, expiredDate: 1});
BookBorrowSchema.index({ status: 1, expiredDate: 1});

export const BookBorrowModel = mongoose.model("BookBorrow", BookBorrowSchema);