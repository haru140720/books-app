import BookService from "../service/book.js";
import { formatResponse } from "../util/base-response.js";

const getAllBook = async (req, res) => {
    const { page = 1, size = 25 } = req.query;
    const books = await BookService.findBooks({ 
        page: +page,
        size: +size,
        query: req.body?.query || {},
        sort: req.body?.sort || {}
    });
    res.json(formatResponse(books));
};

const getBookById = async (req, res) => {
    const { bookId } = req.params;
    const book = await BookService.findBookById(bookId);
    res.json(formatResponse(book));
};

const updateBookById = async (req, res) => {
    const { bookId } = req.params;
    const book = await BookService.updateBookById(bookId, req.body);
    res.json(formatResponse(book));
};

const deleteBookById = async (req, res) => {
    const { bookId } = req.params;
    const book = await BookService.deleteBookById(bookId);
    res.json(formatResponse(book));
};

const searchBookByName = async (req, res) => {
    const { search } = req.body;
    const page = +req.query.page || 1;
    const size = +req.query.size || 25;
    const books = await BookService.searchBooks({search, page, size});
    res.json(formatResponse(books));
}

export default {
    getAllBook,
    getBookById,
    updateBookById,
    deleteBookById,
    searchBookByName
}