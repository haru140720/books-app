import { ERROR_CODE } from "../constant/error-code.js";
import BookBorrowService from "../service/book-borrow.js";
import { formatResponse } from "../util/base-response.js";

const borrowBook = async (req, res) => {
    const { books } = req.body;
    const data = await BookBorrowService.borrowBooksById(books);
    // return error books to client
    if (data?.errorBooks.length > 0) {
        res.status(ERROR_CODE.BAD_REQUEST)
           .send({ status: ERROR_CODE.BAD_REQUEST, data: { errorBooks: data?.errorBooks } });
    }
    res.send(formatResponse(data?.bookBorrow));
};

export default {
    borrowBook
}