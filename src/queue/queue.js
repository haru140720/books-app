import ExpiredReturnQueue from "./expired-return.js";

export const initQueue = () => {
    ExpiredReturnQueue.setUpExpiredReturnQueue();
}