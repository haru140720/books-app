import Queue from "bee-queue";
import axios from "axios";

let expiredReturnQueue = null;

const setUpExpiredReturnQueue = () => {
    const host = process.env.NODE_ENV === "production" ? "books-redis" : "127.0.0.1";
    const REDIS_CONFIG = {
        password: process.env.REDIS_PASSWORD,
        host,
        port: 6379,
    }
    const QUEUE_CONFIG = {
        redis: {
            url: `redis://:${REDIS_CONFIG.password}@${REDIS_CONFIG.host}:${REDIS_CONFIG.port}`
        },
        removeOnSuccess: true,
        removeOnFailure: true,
    };
    // create queue
    expiredReturnQueue = new Queue("EXPIRED_RETURN", QUEUE_CONFIG);
    // process 10 concurrent job
    expiredReturnQueue.process(10, async (job) => {
        try {
            const url = "https://webhook.site/7f608020-7482-4f3d-b4a8-dad006f8fc6b";
            axios.post(url, { message: "You need return books", books: job.data.books });
        } catch (err) {
          console.log(`job ${job.id} exception: ${err.message}`);
        }
    });
}

// publish job
const pushExpiredReturnJob = async ({ books }) => {
    if (expiredReturnQueue) return await expiredReturnQueue.createJob({ books }).save();
    return null;
};

export default {
    setUpExpiredReturnQueue,
    pushExpiredReturnJob
}