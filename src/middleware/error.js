import { ERROR_CODE } from "../constant/error-code.js";

export function errorHandler (err, req, res, next) {
    console.log('a');
    const code = err.code || ERROR_CODE.INTERNAL_SERVER_ERROR;
    let { message } = err;
    switch (code) {
        case ERROR_CODE.BAD_REQUEST:
            message = message || 'Bad Request';
            break;
        case ERROR_CODE.UNAUTHORIZED:
            message = 'Unauthorized';
            break;
        case ERROR_CODE.FORBIDDEN:
            message = 'Forbidden';
            break;
        case ERROR_CODE.NOT_FOUND:
            message = 'Not Found';
            break;
        case ERROR_CODE.TOO_MANY_REQUESTS:
            message = 'Too many requests';
            break;
        case ERROR_CODE.INTERNAL_SERVER_ERROR:
            message = 'Something went wrong';
            break;
        default:
            message = message || 'Something went wrong';
            statusCode = ERROR_CODE.INTERNAL_SERVER_ERROR;
    };
    res.status(code || ERROR_CODE.BAD_REQUEST).send({
        status: code,
        message
    });
}