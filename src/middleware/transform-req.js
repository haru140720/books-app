import camelCase from "camelcase-keys";

export default function (req, res, next) {
    req.query = camelCase(req.query || {}, { deep: true });
    req.body = camelCase(req.body || {}, { deep: true });
    next();
};
  