import { BookModel } from "../model/book.js";
import StringUtil from "../util/string.js";

/**
 * Find books 
 *
 * @param {number} { page } The current page.
 * @param {number} { size } The maximum books of one page
 * @param {object} { sort } The object { field, sortOrder } includes field sort and order to sort (1 or -1)
 * @param {object} { query } The object includes fields to query
 * @return {Promise<array>} array of books
 */
const findBooks = async ({ page, size, sort, query }) => {
    // implement filter or sort 
    /**
     * Code here
     */
    // query 
    return await BookModel.find().skip((page - 1) * size).limit(size);
};

/**
 * Find a book by id
 *
 * @param {string} Id: id of book.
 * @return {Promise<object>} object of a book
 */
const findBookById = async (id) => { 
    return await BookModel.findOne({ Id: id });
};

/**
 * Update a book by id
 *
 * @param {string} bookId: id of book.
 * @param {string} data: the object include fields updated of book
 * @return {Promise<object>} object of updated book
 */
const updateBookById = async (bookId, data) => { 
    // some code here to handle data
    return await BookModel.findOneAndUpdate({ Id: bookId }, { ...data }, { new: true });
};

/**
 * Delete a book by id
 *
 * @param {string} bookId: id of book.
 * @return {string} success if delete successfully
 */
const deleteBookById = async (bookId) => { 
    // some code here to handle data
    await BookModel.deleteOne({ Id: bookId });
    return "success";
};

/**
 * Delete a book by id
 *
 * @param {string} { search }: search text input.
 * @param {number} { page } The current page.
 * @param {number} { size } The maximum books of one page
 * @return {Promise<object>} found books 
 */
const searchBooks = async ({ search, page = 1, size = 25 }) => {
    if (!search) return;
    const words = StringUtil.getWord(search);
    const searchQuery = words.reduce((value, current) => value.concat(`\"${current}\"`), "");
    const books = await BookModel.find({ $text: { $search: searchQuery }}, { score : { $meta: "textScore" } })
                                .sort({ score : { $meta : 'textScore' } })
                                .skip((page - 1) * size)
                                .limit(size);
    return books;
};

export default {
    findBooks,
    findBookById,
    updateBookById,
    deleteBookById,
    searchBooks
}