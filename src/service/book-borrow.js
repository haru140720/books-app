import mongoose from "mongoose";
import { BookModel } from "../model/book.js";
import { BookBorrowModel } from "../model/book-borrow.js";
import { CONSTANT } from "../constant/constant.js";

/**
 * Borrow books by id
 *
 * @param {array<object>} books: { id, amount } - id: id of book, amount: number of borrowed book.
 * @return {Promise<object>} include two field is success borrowed book data and error books
 */
const borrowBooksById = async (books) => { 
    console.log(books);
    if (!books || !Array.isArray(books) || books.length === 0) return;
    // 
    const errorBooks = [], bookItems = [];
    let bookBorrow = {};
    const session = await mongoose.connection.startSession();
    try {
        session.startTransaction();
        await Promise.all(
            books.map(async bookItem => {
                const book = await BookModel.findOne({ Id: bookItem.id });
                if (!book) {
                    errorBooks.push(bookItem);
                    throw new Error("Book not exist");
                }
                // using bookItem.amount checking inventory --> push into errorBooks if not enough and throw error
                bookItems.push({
                    bookId: book.Id,
                    name: book.Name,
                    authors: book.Authors,
                    rating: book.Rating,
                    amount: bookItem.amount
                });
                // change inventory using session
                return;
            })
        );
        bookBorrow = await BookBorrowModel.create({ books: bookItems }, { session });
        console.log(bookItems);
        await session.commitTransaction();
    } catch (e) {
        console.log("Borrow book error:", e);
        await session.abortTransaction();
    };
    console.log(bookBorrow);
    return {
        errorBooks,
        bookBorrow
    };
};

const findExpiredReturn = async (diff =  24 * 60 * 60 * 1000) => {
    const expiredReturns = await BookBorrowModel.find({
        status: CONSTANT.BOOK_BORROW_STATUS.BORROWED,
        expiredDate: {
            $gt: new Date().getTime(),
            $lte:  new Date().getTime() + diff
        }
    });
    return expiredReturns;
}

export default {
    borrowBooksById,
    findExpiredReturn
}