import cron from "node-cron";
import BookBorrowService from "../service/book-borrow.js";
import ExpiredReturnQueue from "../queue/expired-return.js"; 

export const initSchedule = () => {
    // every 12 hour
    cron.schedule("0 0 */12 * * *", async () => {
        // get borrow return books
        const expiredReturns = await BookBorrowService.findExpiredReturn();
        // push to queue
        expiredReturns.forEach(bookReturn => {
            ExpiredReturnQueue.pushExpiredReturnJob({ books: bookReturn });
        });
    });
};