import redis from "redis";

export const initRedis = () => {
    const host = process.env.NODE_ENV === "production" ? "books-redis" : "127.0.0.1";
    const REDIS_CONFIG = {
        password: process.env.REDIS_PASSWORD,
        host,
        port: 6379,
    }

    const client = redis.createClient({
        url: `redis://:${REDIS_CONFIG.password}@${REDIS_CONFIG.host}:${REDIS_CONFIG.port}`
    });
    client.connect();
    // connected
    client.on('connect', () => {
        console.log('Connected Redis!');
    });
    // error
    client.once('error', (err) => {
        console.log('Error Connect Redis:', err);
    });
};