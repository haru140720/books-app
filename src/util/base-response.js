import snakeCase  from "snakecase-keys";

const formatData = (data) => {
    if (!data) return {};

    if (typeof data !== 'object') {
        return data;
    }

    if (Array.isArray(data)) return data;

    // Check if obj is Mongoose Object
    if (data._doc) {
        return data.toJSON();
    }

    if (data._id) delete data._id;

    Object.keys(data).forEach(key => {
        data[key] = formatData(data[key]);
    });

    return snakeCase(data);
}

export const formatResponse = (data) => {
    
    return {
        status: 200,
        data: formatData(data, { deep: true })
    }
};