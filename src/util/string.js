const getWord = (text) => {
    if (!text) return [];
    const words = [];
    while (true) {
        const [word, _text] = text.split(" ");
        words.push(word);
        text = _text;
        if (!_text) break;
    }
    return words;
}

export default {
    getWord
}