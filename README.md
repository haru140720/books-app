BOOKS APP

*Requirement*
1. Dựng restful api cho books -> Hoan thanh
2. Dựng api mượn sách --> Hoan thanh
3. Viết test code (jest hoặc tương đương) --> Chua hoan thanh
4. Viết cronjob bắn webhook khi sách gần đến hạn phải trả (kịch bản báo người mượn trả sách) về https://webhook.site/ (vào đây lấy link) --> Hoan thanh
5. Tạo docker image --> Hoan thanh
6. Tối ưu docker image --> Hoan thanh

*Yêu cầu thêm (nice to have)*

1. Sử dụng Koa js --> Khong su dung
2. Sử dụng bulljs để chạy job --> Em su dung bee-queue thay vi dung bulljs
3. sử dụng typescripts --> Khong su dung
4. Sử dụng jest để test cho các api đã viết --> Chua hoan thanh
5. Sử dụng mongodb với mongoose --> Hoan thanh
6. Dựng api tìm kiếm sách theo tên --> Hoan thanh
7. Viết Ci (Gitlab) --> Hoan thanh mot phan (moi chi viet gitlab-ci.yml, chua setup tren project)