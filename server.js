import * as dotenv from 'dotenv';
import express from 'express';
import morgan from 'morgan';
import connectDb from './src/model/db.js';
import initRouter from './src/routes/routes.js';
import { errorHandler } from './src/middleware/error.js';
import { initRedis } from './src/redis/redis.js';
import { initQueue } from './src/queue/queue.js';
import { initSchedule } from './src/cron/schedule.js';

const app = express();
// config for env
dotenv.config({ path: './.env'});
const PORT = process.env.PORT || 3001;

app.use(morgan(':method -- :url -- :response-time ms'));
app.use(
  express.json({
    extended: true,
    limit: '1mb',
  }),
);

// connect to database
connectDb();
app.use(express.json());
// init redis
initRedis();
// init queue
initQueue();
// init cron
initSchedule();
// init router 
initRouter(app);
app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`Running on PORT ${PORT}`);
});

export default app;